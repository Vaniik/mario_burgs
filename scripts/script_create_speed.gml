//initiate variables
grav=2;
hsp=0;
vsp=0;
jumpspeed=25;
movespeed=10;

//constant
grounded=false;
jumping=false;
key_right=0;
key_left=0;
key_jump=0;
//horizontal jump constants

hsp_jump_constant_small=1;
hsp_jump_constant_big=4
hsp_jump_applied=0;

//horizontal key pressed count
hkp_count=0;
hkp_small=2;
hkp_big=5;
